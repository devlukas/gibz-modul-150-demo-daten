<?php
namespace One50\DemoData\Command;

/*
 * This file is part of the One50.DemoData package.
 */

use One50\Shop\Domain\Model\Category;
use One50\Shop\Domain\Model\Product;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Scope("singleton")
 */
class ShopDataCommandController extends \TYPO3\Flow\Cli\CommandController {
	
	/**
	 * Resource Manager
	 *
	 * @var \TYPO3\Flow\Resource\ResourceManager
	 * @Flow\Inject
	 */
	protected $resourceManager;
	
	/**
	 * Category Repository
	 *
	 * @var \One50\Shop\Domain\Repository\CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * Product Repository
	 *
	 * @var \One50\Shop\Domain\Repository\ProductRepository
	 * @Flow\Inject
	 */
	protected $productRepository;
	
	/**
	 * Import demo data
	 *
	 * Import sample data for categories, products, users and orders
	 */
	public function importCommand() {
		
		$demoData = array(
			'Gemüse'  => array('description' => 'Frisches, einheimisches Gemüse - gesund und schmackhaft',
							   'products'    => array(
								   'Artischocke'      => 'Ein Wunderwerk der Natur: Die erntefrischen Artischocken von der Zürcher Jucker Farm sind nicht nur wunderschön und delikat, sie sind auch überaus gesund. Und das Beste daran: Die Delikatesse ist ganz einfach zubereitet! In Salzwasser mit einem Schuss Zitrone eine Stunde köcheln, eine leckere Vinaigrette dazu – und Blatt für Blatt geniessen.',
								   'Aubergine'        => 'Ob Eierfrucht oder Eierpflanze: Die Aubergine hat viele Namen – und sie ist genau so vielseitig. Gebraten oder gegrillt, gedämpft oder gedünstet: Sie schmeckt immer zart und delikat. Vor allem, wenn sie von den Gebrüder Meier kommt – direkt und frisch aus der Schweizer Erde.',
								   'Broccoli'         => 'Was haben James Bond und die grüne Wolke gemeinsam? Sie wurden von Herrn Broccoli erfunden. Beziehungsweise gezüchtet. Broccoli sind gesund, fein und machen sich auf vielen Tellern beliebt – vor allem, wenn sie natürlich aus der Schweiz stammen, von den Gebrüder Meier.',
								   'Butternut Kürbis' => 'Der Butternuss aka Butternut Kürbis gehört zum Besten, was die Kürbis-Saison zu bieten hat. Denn er kommt nicht nur direkt von den Feldern der Jucker Farm, sondern schmeckt kräftig, leichtsüsslich im Aroma und ist sehr vielfältig verwendbar. Wie wär’s zum Beispiel mit einem Kürbis-Chutney?',
								   'Gurke'            => 'Saure-Gurken-Zeit? Kennen wir nicht. Denn eine frische, knackige Gurke mischt das Gemüsefach so richtig auf. Ursprünglich bezeichnet die Saure-Gurken-Zeit übrigens den Spätsommer, in dem frisch eingelegte Gurken verkauft wurden. Was daran zum Angurken sein soll, ist allerdings unklar.',
								   'Romanesco'        => 'Das schönste Gemüse der Welt: Romanesco ist eine Züchtung aus Blumenkohl und begeistert optisch wie geschmacklich. Und natürlich ist er, wie alle Kohlgemüse, ein Vitamin-Spender par excellence – erntefrisch von regionalen Feldern.',
								   'Salanova grün'    => 'Salanova hat es in sich: Als sogenannter Multiblatt-Kopfsalat hat er dreimal soviele Blätter wie die herkömmliche Variante. Ob er auch dreimal so lecker ist, müssen Sie selber entscheiden. Dieser Salat stammt von BioLand Agrarprodukte und ist zertifiziert von der Bio-Knospe.',
								   'Spinat grob'      => 'Die schlechte Nachricht zuerst: Spinat sollte – einmal gekocht – nicht mehr aufgewärmt werden. Dafür kann man den feinen Spinat auch gerne roh geniessen. Oder leicht angedämpft, mit einer Knoblauchzehe, serviert zu Kartoffeln, Pasta oder Tofu. Eigentlich zu fast allem.',
								   'Tomate'           => 'Frucht, Beere und Gemüse: Die Tomate ist alles zugleich. Und eine Vitamin-Bombe sondergleichen: Von Vitamin A bis E, Folsäure und diversen Mineralien. Roh oder gekocht, gefüllt oder geschmort ist sie der Liebling auf Schweizer Tellern.',
							   )),
			'Kräuter' => array('description' => 'Feine Kräuter zum würzen Ihrer Speisen',
							   'products'    => array(
								   'Basilikum'     => 'Der König unter den Gewürzen: Basilikum. Seine einzigartige Würze und die verdauungsberuhigende Wirkung der grünen Blätter sind im Mittelmeerraum seit der Antike bekannt. Aus schweizer Qualität natürlich von Mäder Kräuter aus Boppelsen ZH. Damit Qualität und Aroma das ganze Jahr garantiert sind, werden wenige Kräuter im Winter im Ausland angebaut, denn nur dort können sie unter natürliche Bedingungen gedeihen.',
								   'Koriander'     => 'Es soll ja Menschen geben, die Koriander nicht ausstehen können. Ist uns unverständlich – vor allem wenn er so frisch und fein daher kommt, wie das würzige Grün von Mäder Kräuter in Boppelsen ZH.',
								   'Peterli kraus' => 'Die krause Petersilie ist das Schweizer Kraut schlechthin - kein SchniPo oder Fischknusperli ohne Peterli-Beilage. Von Mäder Kräuter aus Boppelsen ZH schmeckt der Peterli gleich doppelt so gut - garantiert aus der Schweiz.',
								   'Rosmarin'      => 'Der stachelige Rosmarin kommt aus dem Mittelmeerraum, wächst aber mittlerweile auf jedem zweiten Schweizer Balkon. Zu Recht: Rosmarin bringt eine einzigartige Würze in viele Saucen, Suppen – oder wie wär’s mit Bratkartoffeln und Rosmarin. Unser Rosmrin stammt von Mäder Kräuter in Boppelsen ZH.',
								   'Schnittlauch'  => 'Brot, Butter und gehackter Schnittlauch: Fertig ist das Schnittlauchbrot. Zwar eine bayrische Spezialität, aber wir nehmen sie gerne auf unseren Menuplan. Aus dem Hause von Mäder Kräuter in Boppelsen, schmeckt er gleich doppelt so gut, denn er ist garantiert aus der Schweiz.',
								   'Thymian'       => 'Zur Würze oder gegen Husten: Thymian ist intensiv - und fein. Aus dem Hause von Mäder Kräuter schmeckt er gleich doppelt so gut, denn er ist garantiert aus der Schweiz.',
							   )),
			'Obst'    => array('description' => 'Gesundes Obst aus einheimischer Produktion',
							   'products'    => array(
								   'Apfel Gala'       => 'Viel süss und wenig sauer: Der Gala-Apfel enthält mehr Zucker und weniger Säure als seine fruchtigen Kollegen. Und weil er so schön handlich ist, lieben ihn die Kinder über alles. Gezüchtet wurde er 1934 in Neuseeland – gepflückt aber frisch vom Baum auf der Zürcherischen Jucker Farm.',
								   'Apfel Golden'     => 'Der grüne Klassiker unter den Äpfeln! Süss im Geschmack und knackig im Biss. Bringt Farbe in jeden Fruchtkorb - und Vitamine in Ihren Alltag.',
								   'Apfel Jonagold'   => 'Jonagold - eine der Lieblingssorten der Schweizer Apfel-Fans. Leicht süsslich im Geschmack und knackig frisch. Ideal für Apfelwähen - oder einfach so für Zwischendurch.',
								   'Birne Conference' => 'Die Conference hat wenig Fruchtsäure, aber gleichzeitig einen hohen Fruchtzuckergehalt - typisch Birne. Auch typisch sind die vielen Vitamine und Mineralstoffe. Die Conference weist jedoch eine Besonderheit auf: Während andere Birnen nur reif gegessen werden, ist die Conference auch knackig ganz fein. Ob saftig-süss oder knackig frisch: Die Conference kann beides!',
							   )),
			'Pilze'   => array('description' => 'Schmackhafte Pilze zum Kochen und Braten',
							   'products'    => array(
								   'Champignon braun' => 'Der Franzose: Der Champignon ist der beliebteste Pilz in unseren Breitegraden. Der braune, crèmefarbene Champignon ist etwas stärker im Aroma als sein weisser Bruder. Tipp: Nicht waschen, sondern mit Küchenpapier abwischen. Bei den Bio-Pilzen von Patrick Romanens braucht man sich auch keine Sorgen um chemische Zusätze oder Verunreinigungen zu machen. Einfach geniessen gegart, gegrillt, leicht angedünstet oder roh! Die Champignons werden in Deutschland produziert. "Romanens Pilz" tritt hierbei als Lieferant auf.',
								   'Champignon weiss' => 'Der Franzose: Der Champignon ist der beliebteste Pilz in unseren Breitegraden. Der braune, crèmefarbene Champignon ist etwas stärker im Aroma als sein weisser Bruder. Tipp: Nicht waschen, sondern mit Küchenpapier abwischen. Bei den Bio-Pilzen von Patrick Romanens braucht man sich auch keine Sorgen um chemische Zusätze oder Verunreinigungen zu machen. Einfach geniessen gegart, gegrillt, leicht angedünstet oder roh! Die Champignons werden in Deutschland produziert. "Romanens Pilz" tritt hierbei als Lieferant auf.',
							   ))
		);
		
		foreach ($demoData as $categoryTitle => $categoryData) {
			$category = new Category();
			$category->setTitle($categoryTitle);
			$category->setDescription($categoryData['description']);
			
			foreach ($categoryData['products'] as $productTitle => $productDescription) {
				$product = new Product();
				$product->setTitle($productTitle);
				$product->setDescription($productDescription);
				$product->setCategory($category);
				
				// generate random price
				$price = $this->getRandomPrice();
				$product->setPrice($price);
				
				// set product image
				$imageResource = $this->resourceManager->importResource('resource://One50.DemoData/Public/ProductImages/' . $this->getFilename($product));
				$product->setImage($imageResource);
				
				$this->productRepository->add($product);
			}
			
			$this->categoryRepository->add($category);
		}
	}
	
	/**
	 * Returns a random price
	 *
	 * @return float|int
	 */
	private function getRandomPrice() {
		$factor = rand(19, 100);
		$cents = 5 * $factor;
		
		return floatval($cents / 100);
	}
	
	/**
	 * Generates the filename for a products image
	 *
	 * @param Product $product
	 * @return string
	 */
	private function getFilename(Product $product) {
		$productNameParts = explode(' ', $product->getTitle());
		
		foreach ($productNameParts as &$part) {
			$part = str_replace(array('Ä', 'Ö', 'Ü', 'ä', 'ö', 'ü'), array('Ae', 'Oe', 'Ue', 'ae', 'oe', 'ue'), $part);
			$part = ucfirst(strtolower($part));
		}
		
		$filename = $product->getCategory()->getTitle() . '_' . join('', $productNameParts) . '.jpg';
		
		return $filename;
	}
	
	/**
	 * Clear all data
	 *
	 * Clear all product, category data
	 */
	public function clearCommand() {
		$this->productRepository->removeAll();
		$this->categoryRepository->removeAll();
	}
	
}
